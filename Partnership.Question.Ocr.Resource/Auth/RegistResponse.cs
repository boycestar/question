/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       RegistResponse
     * 命名空间：       Partnership.Question.Ocr.Resource.Auth
     * 创建时间：       5/15/2016 1:59:59 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

namespace Partnership.Question.Ocr.Resource.Auth
{
    /// <summary>
    /// 注册相应对象
    /// </summary>
    public class RegistResponse
    {
        /// <summary>
        /// 认证
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 预留
        /// </summary>
        public string Key { get; set; }
    }
}