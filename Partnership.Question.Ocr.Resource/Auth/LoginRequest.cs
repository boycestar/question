/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       LoginRequest
     * 命名空间：       Partnership.Question.Ocr.Resource.Auth
     * 创建时间：       5/15/2016 2:10:43 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;
using System.ComponentModel.DataAnnotations;

namespace Partnership.Question.Ocr.Resource.Auth
{
    /// <summary>
    /// 用户登陆请求信息
    /// </summary>
    public class LoginRequest
    {
        /// <summary>
        /// 用户名/手机号码
        /// </summary>
        [Required(AllowEmptyStrings = false,ErrorMessage = "登陆用户名/手机号码不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 登陆密码
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "登陆密码不能为空")]
        public string Password { get; set; }

        /// <summary>
        /// 客户端登陆的本地时间
        /// </summary>
        public DateTime LocationTime { get; set; }

        /// <summary>
        /// 设备编号
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// 登陆用的客户端版本
        /// </summary>
        public string AppVersion { get; set; }

        /// <summary>
        /// 客户端登陆的位置
        /// </summary>
        public string Loaction { get; set; }

        /// <summary>
        /// 登陆时使用的 Ip
        /// </summary>
        public string Ip { get; set; }
    }
}