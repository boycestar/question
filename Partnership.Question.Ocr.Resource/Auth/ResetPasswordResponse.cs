/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       ResetPasswordResponse
     * 命名空间：       Partnership.Question.Ocr.Resource.Auth
     * 创建时间：       6/1/2016 11:36:44 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

namespace Partnership.Question.Ocr.Resource.Auth
{
    /// <summary>
    /// 重置密码返回对象
    /// </summary>
    public class ResetPasswordResponse : AuthResponseEntity
    {
    }
}