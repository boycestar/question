/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       ResetPasswordRequest
     * 命名空间：       Partnership.Question.Ocr.Resource.Auth
     * 创建时间：       6/1/2016 11:42:54 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System.ComponentModel.DataAnnotations;

namespace Partnership.Question.Ocr.Resource.Auth
{
    /// <summary>
    /// 重置密码请求对象
    /// </summary>
    public class ResetPasswordRequest
    {
        /// <summary>
        /// 用户名，邮箱，用户名
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "登陆用户不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 新密码
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "新密码不能为空")]
        public string NewPassword { get; set; }
    }
}