/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       CheckUserResponse
     * 命名空间：       Partnership.Question.Ocr.Resource.Auth
     * 创建时间：       6/1/2016 10:32:26 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

namespace Partnership.Question.Ocr.Resource.Auth
{
    /// <summary>
    /// 检测手机号码是否存在
    /// </summary>
    public class CheckUserResponse
    {
        /// <summary>
        /// 是否存在
        /// true：已经存在
        /// false：不存在
        /// </summary>
        public bool IsExists { get; set; }
    }
}