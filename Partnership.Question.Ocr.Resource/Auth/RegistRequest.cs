/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       RegistRequire
     * 命名空间：       Partnership.Question.Ocr.Resource.Auth
     * 创建时间：       5/15/2016 1:46:45 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;
using System.ComponentModel.DataAnnotations;

namespace Partnership.Question.Ocr.Resource.Auth
{
    /// <summary>
    /// 注册用户实体
    /// </summary>
    public class RegistRequest
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public DateTime Birth { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public int Sex { get; set; }

        /// <summary>
        /// 电话号码
        /// </summary>
        [Required(AllowEmptyStrings = false,ErrorMessage = "注册手机号码不能为空")]
        [RegularExpression(@"^[1]+[3,4,5,7,8]+\d{9}", ErrorMessage = "手机号码格式不正确")]
        public string Phone { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required(AllowEmptyStrings = false,ErrorMessage = "注册密码不能为空")]
        public string Password { get; set; }

        /// <summary>
        /// 注册时设备编号
        /// </summary>
        public string RegistDeviceID { get; set; }

        /// <summary>
        /// 注册时设备型号信息
        /// </summary>
        public string RegistDevice { get; set; }

        /// <summary>
        /// 注册时定位位置
        /// </summary>
        public string RegistLocation { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 注册时的科目
        /// </summary>
        public string RegistCourse { get; set; }

        /// <summary>
        /// 注册时， App 的版本号
        /// </summary>
        public string AppVersion { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
    }
}