/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       AuthResponseEntity
     * 命名空间：       Partnership.Question.Ocr.Resource.Auth
     * 创建时间：       6/1/2016 11:01:07 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

namespace Partnership.Question.Ocr.Resource.Auth
{
    /// <summary>
    /// 认证返回对象
    /// </summary>
    public class AuthResponseEntity
    {
        /// <summary>
        /// 凭证
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 剩余次数
        /// </summary>
        public int RestTime { get; set; }

        /// <summary>
        /// 最新版本号
        /// </summary>
        public string ReleaseVersion { get; set; }

        /// <summary>
        /// 预留
        /// </summary>
        public string Key { get; set; }
    }
}