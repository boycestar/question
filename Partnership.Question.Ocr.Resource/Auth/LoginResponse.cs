/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       LoginResponse
     * 命名空间：       Partnership.Question.Ocr.Resource.Auth
     * 创建时间：       5/15/2016 2:17:24 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

namespace Partnership.Question.Ocr.Resource.Auth
{
    /// <summary>
    /// 登陆响应
    /// </summary>
    public class LoginResponse : AuthResponseEntity
    {
    }
}