/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       VerificateCodeRequest
     * 命名空间：       Partnership.Question.Ocr.Resource.VerificationCode
     * 创建时间：       5/25/2016 10:41:10 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System.ComponentModel.DataAnnotations;
using System.IO;

namespace Partnership.Question.Ocr.Resource.VerificationCode
{
    /// <summary>
    /// 验证码验证
    /// </summary>
    public class VerificateCodeRequest
    {
        /// <summary>
        ///  验证码
        /// </summary>
        [RegularExpression(@"^\d{4}$", ErrorMessage = "验证码长度或格式不正确")]
        [Range(1, 9999, ErrorMessage = "验证码长度或格式不正确")]
        public int ValidateCode { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        [RegularExpression(@"^[1]+[3,4,5,7,8]+\d{9}", ErrorMessage = "手机号码格式不正确")]
        [Required(AllowEmptyStrings = false,ErrorMessage = "手机号码不能为空")]
        public string Name { get; set; }
    }
}