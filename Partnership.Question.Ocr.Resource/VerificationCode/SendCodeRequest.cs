/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       VerificationCodeRequest
     * 命名空间：       Partnership.Question.Ocr.Resource.VerificationCode
     * 创建时间：       5/15/2016 2:26:55 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;
using System.ComponentModel.DataAnnotations;

namespace Partnership.Question.Ocr.Resource.VerificationCode
{
    /// <summary>
    /// 验证码请求对象
    /// </summary>
    public class SendCodeRequest
    {
        /// <summary>
        /// 手机号码
        /// </summary>
        [RegularExpression(@"^[1]+[3,4,5,7,8]+\d{9}", ErrorMessage = "手机号码格式不正确")]
        public string Name { get; set; }
    }
}
