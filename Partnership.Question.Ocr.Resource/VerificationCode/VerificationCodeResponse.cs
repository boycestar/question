/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       VerificationCodeResponse
     * 命名空间：       Partnership.Question.Ocr.Resource.VerificationCode
     * 创建时间：       5/15/2016 2:27:21 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

namespace Partnership.Question.Ocr.Resource.VerificationCode
{
    /// <summary>
    /// 验证码是否成功
    /// </summary>
    public class VerificationCodeResponse
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public int IsSuccessful { get; set; }
    }
}