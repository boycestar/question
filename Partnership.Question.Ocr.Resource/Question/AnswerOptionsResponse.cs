/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       AnswerOptionsResponse
     * 命名空间：       Partnership.Question.Ocr.Resource.Question
     * 创建时间：       5/15/2016 1:37:32 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

namespace Partnership.Question.Ocr.Resource.Question
{
    /// <summary>
    /// 答案选项详情
    /// </summary>
    public class AnswerOptionsResponse
    {
        /// <summary>
        /// 答案选项
        /// </summary>
        public string Option { get; set; }

        /// <summary>
        /// 是否为正确答案
        /// </summary>
        public bool IsRight { get; set; }
    }
}