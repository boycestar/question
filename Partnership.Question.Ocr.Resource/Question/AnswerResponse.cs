/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       AnswerResponse
     * 命名空间：       Partnership.Question.Ocr.Resource.Question
     * 创建时间：       5/15/2016 1:35:36 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System.Collections.Generic;

namespace Partnership.Question.Ocr.Resource.Question
{
    /// <summary>
    /// 一道题的题目以及答案
    /// </summary>
    public class AnswerResponse
    {
        /// <summary>
        /// 题目
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 答案项
        /// </summary>
        public IList<AnswerOptionsResponse> Options { get; set; }

        /// <summary>
        /// 问题解析
        /// </summary>
        public string Explain { get; set; }

        /// <summary>
        /// 正确答案
        /// </summary>
        public string RightAnswer { get; set; }
    }
}