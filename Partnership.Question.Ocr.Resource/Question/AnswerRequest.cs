/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       AnswerRequest
     * 命名空间：       Partnership.Question.Ocr.Resource.Question
     * 创建时间：       5/15/2016 1:28:40 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/


using System.ComponentModel.DataAnnotations;
using System.Data.Common;

namespace Partnership.Question.Ocr.Resource.Question
{
    /// <summary>
    /// 请求答案的请求对象
    /// </summary>
    public class AnswerRequest
    {
        /// <summary>
        /// 图片（base 64）
        /// </summary>
        [Required(AllowEmptyStrings = false,ErrorMessage = "图片资源不能为空")]
        public string ImgSource { get; set; }

        /// <summary>
        /// 课程编码
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "课程编码不能为空")]
        public string Course { get; set; }

        /// <summary>
        /// 用户唯一标识/手机号
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "用户不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 预留
        /// </summary>
        public string Key { get; set; }
    }
}