/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       QuestionReponse
     * 命名空间：       Partnership.Question.Ocr.Resource
     * 创建时间：       5/15/2016 2:07:20 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

namespace Partnership.Question.Ocr.Resource
{
    /// <summary>
    /// 响应公共类
    /// </summary>
    public class QuestionReponse<T>
    {
        /// <summary>
        /// 自定义状态码
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 实体数据
        /// </summary>
        public T Data { get; set; }
    }
}