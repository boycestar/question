﻿/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       IEncrypter
     * 命名空间：       Partnership.Question.Ocr.Interface
     * 创建时间：       5/27/2016 11:05:17 PM
     * 作    者：       boyce
     * 邮    箱:        yangbo@eya-it.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

namespace Partnership.Question.Ocr.Interface
{
    public interface IEncrypter
    {
        byte[] Encrypt(byte[] buffer, string strKey);
        byte[] Encrypt(string strInput, string strKey);
        string EncryptAndBase64(string strInput, string strKey);
        string Decrypt(byte[] bInput, string strKey);
        byte[] BinaryDecrypt(byte[] bInput, string strKey);
        string DecryptFromBase64(string strInput, string strKey);
    }
}
