/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       BaseEntity
     * 命名空间：       Partnership.Question.Ocr.Entity
     * 创建时间：       5/28/2016 11:20:08 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;

namespace Partnership.Question.Ocr.Entity
{
    public class BaseEntity
    {
        public string Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreateDate { get; set; }
        public string Creator { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Updater { get; set; }
    }
}