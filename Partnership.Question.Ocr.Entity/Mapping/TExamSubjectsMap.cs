/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       TExamSubjectsMap
     * 命名空间：       Partnership.Question.Ocr.Entity.Mapping
     * 创建时间：       5/22/2016 1:35:18 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System.Data.Entity.ModelConfiguration;
using Partnership.Question.Ocr.Entity.Entity;

namespace Partnership.Question.Ocr.Entity.Mapping
{
    public class TExamSubjectsMap : EntityTypeConfiguration<TExamSubjects>
    {
        public TExamSubjectsMap()
        {
            HasKey(e => e.SbjId);
            Property(e=>e.SbjType).IsRequired();
            Property(e=>e.AnswersCount).IsRequired();
            Property(e=>e.TypeParent).IsRequired();
            Property(e=>e.EDirectoryID).IsRequired();
            Property(e=>e.EDirId).IsRequired();
            Property(e=>e.ESubjectID).IsRequired();
            Property(e=>e.SubjectID1).IsRequired();
            Property(e=>e.SubjectID2).IsRequired();
            Property(e=>e.ProvinceID).IsRequired();
            Property(e=>e.ExplainCount).IsRequired();
            Property(e=>e.xueqi).IsRequired();
            Property(e=>e.xuexiaoid).IsRequired();
            Property(e=>e.jiaoshiid).IsRequired();
            Property(e=>e.kemu1).IsRequired();
            Property(e=>e.kemu2).IsRequired();
            Property(e=>e.kemu3).IsRequired();
            Property(e => e.IsOk).IsRequired();
            Property(e => e.ISAutomatic).IsRequired();
            Property(e => e.CreateTime).IsRequired();
            Property(e => e.UpdateDate);
            Property(e => e.Score).IsRequired();
            Property(e => e.NYear).IsRequired().HasMaxLength(4);
            Property(e => e.Pagetitle).HasMaxLength(255);
            Property(e => e.CreateName).HasMaxLength(255);
            Property(e => e.UpdateUser).HasMaxLength(256);
            Property(e => e.WmaFilePath).HasMaxLength(256);
            Property(e => e.SbjContent);
            Property(e => e.RightAnswer);
            Property(e => e.Explain);
            Property(e => e.SbjTitle);
            ToTable("TExamSubjects");
        }
    }
}