/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       User
     * 命名空间：       Partnership.Question.Ocr.Entity.Entity
     * 创建时间：       5/27/2016 10:45:54 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;

namespace Partnership.Question.Ocr.Entity.Entity
{
    public class User : BaseEntity
    {
        /// <summary>
        ///     姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     生日
        /// </summary>
        public DateTime Birth { get; set; }

        /// <summary>
        ///     性别
        /// </summary>
        public int Sex { get; set; }

        /// <summary>
        ///     电话号码
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        ///     邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     注册时设备编号
        /// </summary>
        public string RegistDeviceID { get; set; }

        /// <summary>
        ///     注册时设备型号信息
        /// </summary>
        public string RegistDevice { get; set; }

        /// <summary>
        ///     注册时定位位置
        /// </summary>
        public string RegistLocation { get; set; }

        /// <summary>
        ///     地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        ///     注册时的科目
        /// </summary>
        public string RegistCourse { get; set; }

        /// <summary>
        ///     注册时， App 的版本号
        /// </summary>
        public string AppVersion { get; set; }

        public bool IsDeleted { get; set; }
    }
}