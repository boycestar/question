/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       TExamSubjects
     * 命名空间：       Partnership.Question.Ocr.Entity.Entity
     * 创建时间：       5/18/2016 11:21:06 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;
using System.ComponentModel.DataAnnotations;

namespace Partnership.Question.Ocr.Entity.Entity
{
    public class TExamSubjects
    {
        [Key]
        public int SbjId { get; set; }
        public int SbjType { get; set; }
        public int AnswersCount { get; set; }
        public int TypeParent { get; set; }
        public int EDirectoryID { get; set; }
        public int EDirId { get; set; }
        public int ESubjectID { get; set; }
        public int SubjectID1 { get; set; }
        public int SubjectID2 { get; set; }
        public int ProvinceID { get; set; }
        public int ExplainCount { get; set; }
        public int xueqi { get; set; }
        public int xuexiaoid { get; set; }
        public int jiaoshiid { get; set; }
        public int kemu1 { get; set; }
        public int kemu2 { get; set; }
        public int kemu3 { get; set; }
        public bool IsOk { get; set; }
        public bool ISAutomatic { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateDate { get; set; }
        public decimal Score { get; set; }
        public string NYear { get; set; }
        public string Pagetitle { get; set; }
        public string CreateName { get; set; }
        public string UpdateUser { get; set; }
        public string WmaFilePath { get; set; }
        public string SbjContent { get; set; }
        public string RightAnswer { get; set; }
        public string Explain { get; set; }
        public string SbjTitle { get; set; }
    }
}