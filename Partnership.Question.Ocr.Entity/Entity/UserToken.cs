/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       UserToken
     * 命名空间：       Partnership.Question.Ocr.Entity.Entity
     * 创建时间：       5/27/2016 10:40:55 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Partnership.Question.Ocr.Entity.Entity
{
    public class UserToken : BaseEntity
    {
        /// <summary>
        ///     用户主键
        /// </summary>
        /// <returns></returns>
        [ForeignKey("User")]
        public string UserId { get; set; }

        /// <summary>
        ///     令牌私钥
        /// </summary>
        /// <returns></returns>
        public string Key { get; set; }

        /// <summary>
        ///     访问令牌
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        ///     客户端类型
        /// </summary>
        /// <returns></returns>
        public int ClientType { get; set; }

        /// <summary>
        ///     过期时间：分钟
        /// </summary>
        /// <returns></returns>
        public int Expire { get; set; }
        public virtual User User { get; set; }
    }
}