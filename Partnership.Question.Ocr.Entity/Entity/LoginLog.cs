/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       LoginLog
     * 命名空间：       Partnership.Question.Ocr.Entity.Entity
     * 创建时间：       5/28/2016 9:54:32 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Partnership.Question.Ocr.Entity.Entity
{
    public class LoginLog : BaseEntity
    {
        /// <summary>
        ///     用户名/手机号码
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     登陆密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     客户端登陆的本地时间
        /// </summary>
        public DateTime LocationTime { get; set; }

        /// <summary>
        ///     设备编号
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        ///     登陆用的客户端版本
        /// </summary>
        public string AppVersion { get; set; }

        /// <summary>
        ///     客户端登陆的位置
        /// </summary>
        public string Loaction { get; set; }

        /// <summary>
        ///     登陆时使用的 Ip
        /// </summary>
        public string Ip { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual User User { get; set; }
    }
}