﻿using System;
using System.Web.Http;
using Partnership.Question.Ocr.Resource;
using Partnership.Question.Ocr.Resource.VerificationCode;
using Partnership.Question.Ocr.Service.Cache;
using Partnership.Question.Ocr.Service.VerificationCode;

namespace Partnership.Question.Ocr.WebApi.Controllers
{
    /// <summary>
    ///     验证码相关控制器
    /// </summary>
    public class VerificationCodeController : ApiController
    {
        /// <summary>
        ///     发送验证码
        /// </summary>
        /// <param name="request">请求对象</param>
        /// <returns>相应对象</returns>
        [HttpPost]
        [Route("api/Message/Send")]
        public QuestionReponse<VerificationCodeResponse> Send(SendCodeRequest request)
        {
            return new QuestionReponse<VerificationCodeResponse>
            {
                Data = new VerificationCodeResponse
                {
                    IsSuccessful = 0
                },
                State = 3300,
                Message = "5分钟内不能重复发送验证码"
            };


            var redisCache = new RedisCache("127.0.0.1", 6379);
            var code = redisCache.Get<string>(request.Name);

            if (!string.IsNullOrWhiteSpace(code))
            {
                return new QuestionReponse<VerificationCodeResponse>
                {
                    Data = new VerificationCodeResponse
                    {
                        IsSuccessful = 0
                    },
                    State = 3300,
                    Message = "5分钟内不能重复发送验证码"
                };
            }

            var sendCountKey = string.Format("{0}{1}", request.Name, DateTime.Now.ToString("yyyyMMdd"));
            var sendCount = redisCache.Get<long>(sendCountKey);
            redisCache.IncreaseOne(sendCountKey);
            if (sendCount > ServiceStack.Configuration.ConfigUtils.GetAppSetting("MESSGECOUNT", 10))
            {
                return new QuestionReponse<VerificationCodeResponse>
                {
                    Data = new VerificationCodeResponse
                    {
                        IsSuccessful = 0
                    },
                    State = 3300,
                    Message = "今天短信使用次数已经用完"
                };
            }

            var generateCode = SendMessage.GenerateCode();
            var result = SendMessage.Send(request.Name, generateCode);
            if (result.IndexOf("ok", StringComparison.OrdinalIgnoreCase) < 0)
            {
                return new QuestionReponse<VerificationCodeResponse>
                {
                    Data = new VerificationCodeResponse
                    {
                        IsSuccessful = 0
                    },
                    State = 3300,
                    Message = "验证码发送失败"
                };
            }
            var cacheRedis = redisCache.Insert(request.Name, generateCode, 5);
            if (cacheRedis)
            {
                redisCache.IncreaseOne(sendCountKey);
                return new QuestionReponse<VerificationCodeResponse>
                {
                    Data = new VerificationCodeResponse
                    {
                        IsSuccessful = 1
                    },
                    State = 3200,
                    Message = "请求成功"
                };
            }

            return new QuestionReponse<VerificationCodeResponse>
            {
                Data = new VerificationCodeResponse
                {
                    IsSuccessful = 0
                },
                State = 3300,
                Message = "请求失败"
            };
        }

        /// <summary>
        ///     验证验证码
        /// </summary>
        /// <param name="request">请求对象</param>
        /// <returns>相应对象</returns>
        [HttpPost]
        [Route("api/Message/Validate")]
        public QuestionReponse<VerificationCodeResponse> Validator(VerificateCodeRequest request)
        {

            return new QuestionReponse<VerificationCodeResponse>
            {
                Data = new VerificationCodeResponse
                {
                    IsSuccessful = 1
                },
                State = 3200,
                Message = "请求成功"
            };


            var redisCache = new RedisCache("127.0.0.1", 6379);
            var code = redisCache.Get<string>(request.Name);
            if (!string.IsNullOrWhiteSpace(code) && code == request.ValidateCode.ToString())
            {
                return new QuestionReponse<VerificationCodeResponse>
                {
                    Data = new VerificationCodeResponse
                    {
                        IsSuccessful = 1
                    },
                    State = 3200,
                    Message = "请求成功"
                };
            }

            return new QuestionReponse<VerificationCodeResponse>
            {
                Data = new VerificationCodeResponse
                {
                    IsSuccessful = 0
                },
                State = 3300,
                Message = "请求失败"
            };
        }
    }
}