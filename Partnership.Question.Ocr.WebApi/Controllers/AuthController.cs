﻿using System.Text.RegularExpressions;
using System.Web.Http;
using Partnership.Question.Ocr.Resource;
using Partnership.Question.Ocr.Resource.Auth;
using Partnership.Question.Ocr.Service.Auth;
using Partnership.Question.Ocr.Service.Encrypter;

namespace Partnership.Question.Ocr.WebApi.Controllers
{
    /// <summary>
    ///     用户认证控制器
    /// </summary>
    public class AuthController : ApiController
    {
        /// <summary>
        ///     用户注册接口
        /// </summary>
        /// <param name="request">请求对象</param>
        /// <returns>响应对象</returns>
        [HttpPost]
        [Route("api/Auth/Regist")]
        public QuestionReponse<RegistResponse> Regist(RegistRequest request)
        {
            var user = new RegisterService().Get(request.Name, request.Phone, request.Email);
            if (user != null)
                return new QuestionReponse<RegistResponse>
                {
                    Data = null,
                    Message = "用户名或手机号码或邮箱重复",
                    State = 3300
                };
            var userOperation = new UserOperation();
            var registResponse = userOperation.RegisterUser(request);
            return new QuestionReponse<RegistResponse>
            {
                Data = registResponse,
                Message = "请求成功",
                State = 3200
            };
        }

        /// <summary>
        ///     用户登陆接口
        /// </summary>
        /// <param name="request">请求对象</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Auth/Login")]
        public QuestionReponse<LoginResponse> Login(LoginRequest request)
        {
            var registerService = new RegisterService();
            var user = registerService.Get(request.Name, new Md5Encrypter().Get32Md5(request.Password));
            if (user == null)
                return new QuestionReponse<LoginResponse>
                {
                    Data = null,
                    State = 3300,
                    Message = "用户名或密码错误"
                };
            var userOperation = new UserOperation();
            var loginResponse = userOperation.Login(request, user.Id);

            if (loginResponse != null)
            {
                return new QuestionReponse<LoginResponse>
                {
                    Data = new LoginResponse
                    {
                        Key = loginResponse.Key,
                        Token = loginResponse.Token
                    },
                    Message = "请求成功",
                    State = 3200
                };
            }
            return new QuestionReponse<LoginResponse>
            {
                Data = null,
                State = 3300,
                Message = "用户名或密码错误"
            };
        }

        /// <summary>
        /// 手机号码手否已经存在
        /// </summary>
        /// <param name="phone">手机号码</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Auth/Check")]
        public QuestionReponse<CheckUserResponse> Check(string phone)
        {
            var regex = new Regex(@"^[1][3-8]\d{9}$");
            if(string.IsNullOrWhiteSpace(phone) || !regex.IsMatch(phone))
                return new QuestionReponse<CheckUserResponse>()
                {
                    Data = null,
                    Message = "手机号码不能为空，或手机格式不正确",
                    State = 3300
                };

            var user = new RegisterService().Get(phone);
            if (user == null)
            {
                return new QuestionReponse<CheckUserResponse>()
                {
                    Data = new CheckUserResponse()
                    {
                        IsExists = false
                    },
                    Message = "手机号码不存在",
                    State = 3200
                };
            }
            else
            {
                return new QuestionReponse<CheckUserResponse>()
                {
                    Data = new CheckUserResponse()
                    {
                        IsExists = true
                    },
                    Message = "手机号码已经存在",
                    State = 3200
                };
            }
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Auth/Password/Change")]
        public QuestionReponse<ChangePasswordResponse> ChangrPassword(ChangeRequest request)
        {
            var registerService = new RegisterService();
            var md5Encrypter = new Md5Encrypter();
            var user = registerService.Get(request.Name, md5Encrypter.Get32Md5(request.OldPassword));
            if (user == null)
                return new QuestionReponse<ChangePasswordResponse>
                {
                    Data = null,
                    State = 3300,
                    Message = "用户名或密码错误"
                };

            var newPassword = md5Encrypter.Get32Md5(request.NewPassword);
            user.Password = newPassword;
            registerService.Update(user);

            var userOperation = new UserOperation();
            var loginResponse = userOperation.ChangePassword(user);

            if (loginResponse != null)
            {
                return new QuestionReponse<ChangePasswordResponse>
                {
                    Data = new ChangePasswordResponse
                    {
                        Key = loginResponse.Key,
                        Token = loginResponse.Token
                    },
                    Message = "请求成功",
                    State = 3200
                };
            }
            return new QuestionReponse<ChangePasswordResponse>
            {
                Data = null,
                State = 3300,
                Message = "生成认证失败，请重新登陆"
            };
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Auth/Password/Reset")]
        public QuestionReponse<ResetPasswordResponse> ResetPassword(ResetPasswordRequest request)
        {
            var registerService = new RegisterService();
            var md5Encrypter = new Md5Encrypter();
            var user = registerService.Get(request.Name);
            if (user == null)
                return new QuestionReponse<ResetPasswordResponse>
                {
                    Data = null,
                    State = 3300,
                    Message = "用户名不正确"
                };

            var newPassword = md5Encrypter.Get32Md5(request.NewPassword);
            user.Password = newPassword;
            registerService.Update(user);

            var userOperation = new UserOperation();
            var loginResponse = userOperation.ChangePassword(user);

            if (loginResponse != null)
            {
                return new QuestionReponse<ResetPasswordResponse>
                {
                    Data = new ResetPasswordResponse
                    {
                        Key = loginResponse.Key,
                        Token = loginResponse.Token
                    },
                    Message = "请求成功",
                    State = 3200
                };
            }
            return new QuestionReponse<ResetPasswordResponse>
            {
                Data = null,
                State = 3300,
                Message = "生成认证失败，请重新重置密码"
            };
        }
    }
}