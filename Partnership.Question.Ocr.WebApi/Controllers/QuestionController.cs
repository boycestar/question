﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web.Http;
using Partnership.Question.Ocr.Entity.Entity;
using Partnership.Question.Ocr.Resource;
using Partnership.Question.Ocr.Resource.Question;
using Partnership.Question.Ocr.Service.Auth;
using Partnership.Question.Ocr.Service.Search;
using ServiceStack.Configuration;

namespace Partnership.Question.Ocr.WebApi.Controllers
{
    /// <summary>
    ///     查询答案
    /// </summary>
    public class QuestionController : ApiController
    {
        private readonly List<string> Selector = new List<string> {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};

        /// <summary>
        ///     请求答案
        /// </summary>
        /// <param name="request">请求参数，base64 图片信息</param>
        /// <returns>答案列表</returns>
        [HttpPost]
        public QuestionReponse<IList<AnswerResponse>> Answer(AnswerRequest request)
        {
            var validAuth = ValidAuth();
            if (!validAuth)
                return new QuestionReponse<IList<AnswerResponse>>()
                {
                    Data = null,
                    Message = "非法访问",
                    State = 3300
                };
            
            var answerResponses = GetQuestionReponse(request.ImgSource);
            return new QuestionReponse<IList<AnswerResponse>>
            {
                Data = answerResponses,
                Message = "请求成功",
                State = 3200
            };
        }

        private IList<AnswerResponse> GetQuestionReponse(string imgSource)
        {
            var imageText = GetImageText(imgSource);
            var total = 0;
            var luceneSearch = new LuceneSearch();
            var searchExamSubjects = luceneSearch.SearchExamSubjects(imageText, 1, 6, out total);
            return ConvertToAnswerResponse(searchExamSubjects);
        }

        /// <summary>
        ///     过滤中文
        /// </summary>
        /// <param name="imgSource"></param>
        /// <returns></returns>
        private string GetImageText(string imgSource)
        {
            var fileName = Guid.NewGuid().ToString().Replace("-", "");

            var fullPath = SaveImge(imgSource, fileName);

            var resultFullPath = OcrTextFile(fileName, fullPath);

            if (string.IsNullOrWhiteSpace(resultFullPath)) return string.Empty;

            var textChineseContent = GetTextChineseContent(resultFullPath);
            return textChineseContent;
        }

        private string GetTextChineseContent(string resultFullPath)
        {
            using (var streamReader = new StreamReader(resultFullPath))
            {
                var readToEnd = streamReader.ReadToEnd();
                var chars = readToEnd.ToCharArray();

                var chineseString = new StringBuilder();
                foreach (var ch in chars)
                {
                    if (ch >= 0x4e00 && ch <= 0x9fbb)
                    {
                        chineseString.Append(ch);
                    }
                }

                return chineseString.ToString();
            }
        }

        private string OcrTextFile(string fileName, string fullPath)
        {
            var resultPath = Path.Combine(ConfigUtils.GetAppSetting("RESULTPATH"), fileName);
            var tesseractPath = ConfigUtils.GetAppSetting("TESSERACTPATH");
            var lang = ConfigUtils.GetAppSetting("LANG");
            var arguments = string.Format(@"{0} {1} {2} -l {3}", tesseractPath, fullPath, resultPath, lang);
            try
            {
                using (var si = new Process())
                {
                    si.StartInfo.WorkingDirectory = "c:\\";
                    si.StartInfo.UseShellExecute = false;
                    si.StartInfo.FileName = "cmd.exe";
                    si.StartInfo.Arguments = string.Format("/c {0}", arguments);
                    si.StartInfo.CreateNoWindow = true;
                    si.StartInfo.RedirectStandardInput = true;
                    si.StartInfo.RedirectStandardOutput = true;
                    si.StartInfo.RedirectStandardError = true;
                    si.Start();
                    si.Close();
                }
            }
            catch (Exception ex)
            {
            }

            var resultFullPath = string.Format("{0}.txt", resultPath);
            var index = 0;
            var exists = false;

            while (!exists)
            {
                exists = File.Exists(resultFullPath);
                index ++;
                if (index > 50)
                {
                    break;
                }

                Thread.Sleep(300);
            }
            return !exists ? string.Empty : resultFullPath;
        }

        private string SaveImge(string imgSource, string fileName)
        {
            var bytes = Convert.FromBase64String(imgSource);
            var imagePath = ConfigUtils.GetAppSetting("IMAGEPATH");


            var fullPath = string.Format("{0}\\{1}.jpg", imagePath, fileName);
            using (var imageFile = new FileStream(fullPath, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }
            return fullPath;
        }

        private IList<AnswerResponse> ConvertToAnswerResponse(IList<TExamSubjects> entities)
        {
            var response = new List<AnswerResponse>();
            if (entities == null || !entities.Any())
                return response;

            foreach (var entity in entities)
            {
                if (string.IsNullOrWhiteSpace(entity.SbjContent))
                    continue;
                try
                {
                    var sbjContent = entity.SbjContent.Split(new[] {@"$t_m$$x-x$"}, StringSplitOptions.None);

                    var options = sbjContent[1].Split(new[] {@"$x_x$$x-x$"}, StringSplitOptions.None);

                    var answerResponse = new AnswerResponse();
                    answerResponse.Explain = entity.Explain;
                    answerResponse.Options = ConvertToAnswerOptionsResponse(options, entity.RightAnswer);
                    answerResponse.Title =
                        sbjContent[0].Replace("$t-m$", "").Replace("<p>", "").Replace("</p>", "").Trim(' ');
                    answerResponse.RightAnswer = entity.RightAnswer;
                    response.Add(answerResponse);
                }
                catch (Exception ex)
                {
                }
            }

            return response;
        }

        private IList<AnswerOptionsResponse> ConvertToAnswerOptionsResponse(IList<string> options, string rightAnswer)
        {
            var answerOptionsResponses = new List<AnswerOptionsResponse>();
            var length = options.Count - 1;
            for (var i = 0; i < length; i++)
            {
                var answerOptionsResponse = new AnswerOptionsResponse();
                answerOptionsResponse.Option = string.Format("{0} {1}", Selector[i], options[i].Replace("$x_x$", ""));
                answerOptionsResponse.IsRight = rightAnswer.IndexOf(Selector[i], StringComparison.OrdinalIgnoreCase) >=
                                                0;
                answerOptionsResponses.Add(answerOptionsResponse);
            }

            return answerOptionsResponses;
        }

        private bool ValidAuth()
        {
            var token1 = GetHeaderValue(Request, "Token");
            var key1 = GetHeaderValue(Request, "Key");

            var userToken = new UserTokenService().Get(key1, token1);
            return userToken != null;
        }

        private string GetHeaderValue(HttpRequestMessage request, string name)
        {
            IEnumerable<string> values;
            var found = request.Headers.TryGetValues(name, out values);
            if (found)
            {
                return values.FirstOrDefault();
            }

            return null;
        }
    }
}