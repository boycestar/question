﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Lucene.Net.Analysis.PanGu;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Partnership.Question.Ocr.Repository;
using ServiceStack.Configuration;

namespace Partnership.Question.Ocr.Console.SerachIndex
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var indexStorePath = ConfigUtils.GetAppSetting("INDEX_STORE_PATH");
            if (System.IO.Directory.Exists(indexStorePath))
            {
                System.IO.Directory.Delete(indexStorePath, true);
            }
            var sw = new Stopwatch(); //加入时间统计
            //获取 数据列表
            using (var ctx = new DatabasesContext())
            {
                var examSubjectses = ctx.ExamSubjectses.ToList();

                //创建Lucene索引文件
                sw.Start();
                var writer = new IndexWriter(FSDirectory.Open(indexStorePath), new PanGuAnalyzer(), true,
                    IndexWriter.MaxFieldLength.LIMITED);
                foreach (var item in examSubjectses)
                {
                    var doc = new Document();
                    var sbjId = new Field("SbjId", item.SbjId.ToString(), Field.Store.YES, Field.Index.ANALYZED,
                        Field.TermVector.NO);
                    var sbjContent = new Field("SbjContent", item.SbjContent, Field.Store.YES, Field.Index.ANALYZED,
                        Field.TermVector.NO);
                    var rightAnswer = new Field("RightAnswer", item.RightAnswer, Field.Store.YES, Field.Index.ANALYZED,
                        Field.TermVector.NO);
                    var explain = new Field("Explain", item.Explain, Field.Store.YES, Field.Index.ANALYZED,
                        Field.TermVector.NO);
                    doc.Add(sbjId);
                    doc.Add(sbjContent);
                    doc.Add(rightAnswer);
                    doc.Add(explain);
                    writer.AddDocument(doc);
                }
                writer.Optimize();
                writer.Commit();
                sw.Stop();
                System.Console.Write("建立" + examSubjectses.Count + "索引,花费: " + sw.Elapsed);
                System.Console.ReadLine();
            }
        }

        public static string GetKeyWordsSplitBySpace(string keywords, PanGuTokenizer ktTokenizer)
        {
            var result = new StringBuilder();
            /*执行分词操作 一个关键字可以拆分为多个次和单个字*/
            var words = ktTokenizer.SegmentToWordInfos(keywords);

            foreach (var word in words)
            {
                if (word == null)
                {
                    continue;
                }

                result.AppendFormat("{0} ", word.Word);
            }

            return result.ToString().Trim();
        }

        public static ScoreDoc[] TopDocs(int start, int limit, TopFieldDocs docs)
        {
            var endIndex = 0;
            var hc = docs.TotalHits;
            if (hc - start > limit)
            {
                endIndex = start + limit;
            }
            else
            {
                endIndex = hc;
            }

            var dl = new List<ScoreDoc>();
            var da = docs.ScoreDocs;
            for (var i = start; i < endIndex; i++)
            {
                dl.Add(da[i]);
            }
            return dl.ToArray();
        }
    }
}