/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       DatabasesContext
     * 命名空间：       Partnership.Question.Ocr.Repository
     * 创建时间：       5/22/2016 2:26:34 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System.Data.Entity;
using Partnership.Question.Ocr.Entity.Entity;

namespace Partnership.Question.Ocr.Repository
{
    public class DatabasesContext:DbContext
    {
        public DatabasesContext()
            : base("name=DbConnectionString")
        {
            // Database.SetInitializer(new CreateDatabaseIfNotExists<DatabasesContext>());
        }

        public DbSet<TExamSubjects> ExamSubjectses
        {
            get; set;
            
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserToken> UserTokens { get; set; }
        public DbSet<LoginLog> LoginLogs { get; set; }
    }
}