/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       RegisterRepository
     * 命名空间：       Partnership.Question.Ocr.Repository.Auth
     * 创建时间：       5/26/2016 7:08:49 AM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System.Data.Entity.Migrations;
using System.Linq;
using Partnership.Question.Ocr.Entity.Entity;
namespace Partnership.Question.Ocr.Repository.Auth
{
    public class RegisterRepository
    {
        public User Add(User enity)
        {

            if (string.IsNullOrWhiteSpace(enity.Phone) || string.IsNullOrWhiteSpace(enity.Password))
                return null;

            using (var ctx = new DatabasesContext())
            {
                var user = ctx.Users.Add(enity);
                var result = ctx.SaveChanges();
                return result > 0 ? user : null;
            }
        }

        public User Get(string name, string password)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(password))
                return null;

            using (var ctx = new DatabasesContext())
            {
                return
                    ctx.Users.FirstOrDefault(
                        e => (e.Name == name || e.Phone == name || e.Email == name) && e.Password == password);
            }
        }

        public User Get(string name)
        {
            using (var ctx = new DatabasesContext())
            {
                return
                    ctx.Users.FirstOrDefault(
                        e => (e.Name == name || e.Phone == name || e.Email == name));
            }
        }

        public User Get(string name, string phone, string email)
        {
            using (var ctx = new DatabasesContext())
            {
                return
                    ctx.Users.FirstOrDefault(
                        e =>
                            (e.Name == name && name !="") || (e.Phone == phone && phone != "") || (e.Email == email && email != ""));
            }
        }

        public User Update(User user)
        {
            using (var ctx = new DatabasesContext())
            {
                ctx.Users.AddOrUpdate(user);
                var result = ctx.SaveChanges();
                return result > 0 ? user : null;
            }
        }
    }
}