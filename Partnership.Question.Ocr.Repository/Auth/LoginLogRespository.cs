/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       LoginLogRespository
     * 命名空间：       Partnership.Question.Ocr.Repository.Auth
     * 创建时间：       5/28/2016 11:50:26 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System.Data.Entity;
using System.Linq;
using Partnership.Question.Ocr.Entity.Entity;

namespace Partnership.Question.Ocr.Repository.Auth
{
    public class LoginLogRespository
    {

        public LoginLog Add(LoginLog enity)
        {
            using (var ctx = new DatabasesContext())
            {
                if (string.IsNullOrWhiteSpace(enity.Name) || string.IsNullOrWhiteSpace(enity.Password))
                    return null;

                var loginLog = ctx.LoginLogs.Add(enity);

                var result = ctx.SaveChanges();
                return result > 0 ? loginLog : null;
            }
        }

        public LoginLog Get(string userId)
        {
            using (var ctx = new DatabasesContext())
            {
                return ctx.LoginLogs.FirstOrDefault(e => e.UserId == userId);
            }
        }
    }
}