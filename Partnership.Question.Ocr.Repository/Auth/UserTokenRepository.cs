/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       UserTokenRepository
     * 命名空间：       Partnership.Question.Ocr.Repository.Auth
     * 创建时间：       5/27/2016 10:59:35 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System.Data.Entity.Migrations;
using System.Linq;
using Partnership.Question.Ocr.Entity.Entity;

namespace Partnership.Question.Ocr.Repository.Auth
{
    public class UserTokenRepository
    {
        public UserToken Add(UserToken enity)
        {
            if (string.IsNullOrWhiteSpace(enity.UserId) || string.IsNullOrWhiteSpace(enity.Key))
                return null;

            using (var ctx = new DatabasesContext())
            {
                var userToken = ctx.UserTokens.Add(enity);
                var result = ctx.SaveChanges();
                return result > 0 ? userToken : null;
            }
        }

        public UserToken Update(UserToken enity)
        {
            if (string.IsNullOrWhiteSpace(enity.UserId) || string.IsNullOrWhiteSpace(enity.Key))
                return null;

            using (var ctx = new DatabasesContext())
            {
                ctx.UserTokens.AddOrUpdate(enity);
                var result = ctx.SaveChanges();
                return result > 0 ? enity : null;
            }
        }

        public UserToken Get(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return null;

            using (var ctx = new DatabasesContext())
            {
                return
                    ctx.UserTokens.FirstOrDefault(
                        e => e.User.Name == name || e.User.Phone == name || e.User.Email == name);
            }
        }

        public UserToken Get(string key,string token)
        {
            if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(token))
                return null;

            using (var ctx = new DatabasesContext())
            {
                return
                    ctx.UserTokens.FirstOrDefault(
                        e => e.Key == key && e.Token == token);
            }
        }
    }
}