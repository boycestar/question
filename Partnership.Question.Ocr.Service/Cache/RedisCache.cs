/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       RedisCache
     * 命名空间：       Partnership.Question.Ocr.Service.Cache
     * 创建时间：       5/25/2016 10:00:15 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using ServiceStack.Redis;
using System;
using System.Linq;

namespace Partnership.Question.Ocr.Service.Cache
{
    public class RedisCache
    {
        public RedisClient RedisClient { get; private set; }
        private readonly object _object = new object();

        public RedisCache()
        {
            RedisClient = new RedisClient();
        }

        public RedisCache(string host, int post)
        {
            RedisClient = new RedisClient(host, post);
        }

        public RedisCache(string host, int post, string password)
        {
            RedisClient = new RedisClient(host, post, password);
        }


        /// <summary>
        /// 添加键值到 Redis， 如果 Key 已经存在，将不会更新 Value
        /// </summary>
        /// <typeparam name="T">添加的对象类型</typeparam>
        /// <param name="argKey">缓存的键</param>
        /// <param name="argValue">缓存的值</param>
        /// <returns>true：添加成功 false：添加失败</returns>
        public bool Add<T>(string argKey, T argValue) 
        {
            lock (_object)
            {
                return RedisClient.Add(argKey, argValue);
            }
        }

        /// <summary>
        /// 添加键值到 Redis， 如果 Key 已经存在，将不会更新 Value，仅仅更新缓存时长
        /// </summary>
        /// <typeparam name="T">添加的对象类型</typeparam>
        /// <param name="argKey">缓存的键</param>
        /// <param name="argValue">缓存的值</param>
        /// <param name="expiration">缓存的时长（单位：分钟）</param>
        /// <returns>true：添加成功 false：添加失败</returns>
        public bool Add<T>(string argKey, T argValue, int expiration)
        {
            lock (_object)
            {
                return RedisClient.Exists(argKey) == 1
                    ? RedisClient.Expire(argKey, 60 * expiration)
                    : RedisClient.Add(argKey, argValue, DateTime.Now.AddMinutes(expiration));
            }
        }

        /// <summary>
        /// 添加键值到 Redis， 如果 Key 已经存在，将不会更新 Value，仅仅更新缓存时长
        /// </summary>
        /// <typeparam name="T">添加的对象类型</typeparam>
        /// <param name="argKey">缓存的键</param>
        /// <param name="argValue">缓存的值</param>
        /// <param name="expiration">缓存的时长（单位：秒）</param>
        /// <returns>true：添加成功 false：添加失败</returns>
        public bool AddBySecond<T>(string argKey, T argValue, int expiration)
        {
            lock (_object)
            {
                return RedisClient.Exists(argKey) == 1
                    ? RedisClient.Expire(argKey, expiration)
                    : RedisClient.Add(argKey, argValue, DateTime.Now.AddSeconds(expiration));
            }
        }

        /// <summary>
        /// 添加键值到 Redis， 如果 Key 已经存在，将会更新 Value
        /// </summary>
        /// <typeparam name="T">添加的对象类型</typeparam>
        /// <param name="argKey">缓存的键</param>
        /// <param name="argValue">缓存的值</param>
        /// <returns>true：添加成功 false：添加失败</returns>
        public bool Insert<T>(string argKey, T argValue)
        {
            lock (_object)
            {
                return RedisClient.Set(argKey, argValue);
            }
        }

        /// <summary>
        /// 添加键值到 Redis， 如果 Key 已经存在，将会更新 Value 以及缓时间
        /// </summary>
        /// <typeparam name="T">添加的对象类型</typeparam>
        /// <param name="argKey">缓存的键</param>
        /// <param name="argValue">缓存的值</param>
        /// <param name="expiration">缓存的时长（单位：分钟）</param>
        /// <returns>true：添加成功 false：添加失败</returns>
        public bool Insert<T>(string argKey, T argValue, int expiration)
        {
            lock (_object)
            {
                return RedisClient.Set(argKey, argValue, DateTime.Now.AddMinutes(expiration));
            }
        }

        /// <summary>
        /// 添加键值到 Redis， 如果 Key 已经存在，将会更新 Value 以及缓时间
        /// </summary>
        /// <typeparam name="T">添加的对象类型</typeparam>
        /// <param name="argKey">缓存的键</param>
        /// <param name="argValue">缓存的值</param>
        /// <param name="expiration">缓存的时长（单位：秒）</param>
        /// <returns>true：添加成功 false：添加失败</returns>
        public bool InsertBySecond<T>(string argKey, T argValue, int expiration)
        {
            lock (_object)
            {
                return RedisClient.Set(argKey, argValue, DateTime.Now.AddSeconds(expiration));
            }
        }

        /// <summary>
        /// 将 Key 的缓存移除
        /// </summary>
        /// <param name="argKey">要移除的 Key</param>
        /// <returns>true：移除成功 false：移除失败</returns>
        public bool Remove(string argKey)
        {
            lock (_object)
            {
                return RedisClient.Remove(argKey);
            }
        }

        /// <summary>
        /// 添加键值到 Redis， 如果 Key 已经存在，将会更新 Value
        /// </summary>
        /// <typeparam name="T">添加的对象类型</typeparam>
        /// <param name="argKey">缓存的键</param>
        /// <param name="argValue">缓存的值</param>
        /// <returns>true：添加成功 false：添加失败</returns>
        public bool CacheData<T>(string argKey, T argValue)
        {
            return Insert(argKey, argValue);
        }

        /// <summary>
        /// 添加键值到 Redis， 如果 Key 已经存在，将会更新 Value 以及缓时间
        /// </summary>
        /// <typeparam name="T">添加的对象类型</typeparam>
        /// <param name="argKey">缓存的键</param>
        /// <param name="argValue">缓存的值</param>
        /// <param name="expiration">缓存的时长（单位：分钟）</param>
        /// <returns>true：添加成功 false：添加失败</returns>
        public bool CacheData<T>(string argKey, T argValue, int expiration)
        {
            return Insert(argKey, argValue, expiration);
        }

        /// <summary>
        /// 添加键值到 Redis， 如果 Key 已经存在，将会更新 Value 以及缓时间
        /// </summary>
        /// <typeparam name="T">添加的对象类型</typeparam>
        /// <param name="argKey">缓存的键</param>
        /// <param name="argValue">缓存的值</param>
        /// <param name="expiration">缓存的时长（单位：秒）</param>
        /// <returns>true：添加成功 false：添加失败</returns>
        public bool CacheDataBySecond<T>(string argKey, T argValue, int expiration)
        {
            return InsertBySecond(argKey, argValue, expiration);
        }

        /// <summary>
        /// 添加键值到 Redis， 如果 Key 已经存在，将会更新 Value
        /// </summary>
        /// <typeparam name="T">添加的对象类型</typeparam>
        /// <param name="argKey">缓存的键</param>
        /// <param name="argValue">缓存的值</param>
        /// <param name="timespan">过期的 Timespan</param>
        /// <returns>true：添加成功 false：添加失败</returns>
        public bool CacheData<T>(string argKey, T argValue, TimeSpan timespan)
        {
            lock (_object)
            {
                return RedisClient.Set(argKey, argValue, timespan);
            }
        }

        /// <summary>
        /// 根据键获取缓存 Value
        /// </summary>
        /// <typeparam name="T">泛型对象</typeparam>
        /// <param name="argKey">缓存的 Key</param>
        /// <returns>泛型对象</returns>
        public T Get<T>(string argKey)
        {
            return RedisClient.Get<T>(argKey);
        }

        /// <summary>
        /// 根据键获取缓存 Value
        /// </summary>
        /// <typeparam name="T">泛型对象</typeparam>
        /// <param name="argKey">缓存的 Key</param>
        /// <returns>泛型对象</returns>
        public T GetData<T>(string key)
        {
            return Get<T>(key);
        }

        /// <summary>
        /// 清空整个 ReidsDB
        /// </summary>
        public void ClearAll()
        {
            lock (_object)
            {
                RedisClient.FlushAll();
            }
        }


        /// <summary>
        /// 获取当前 RedisDB 的所有 Values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Value 数组</returns>
        public T[] GetValues<T>()
        {
            var keys = RedisClient.GetAllKeys();
            if (keys == null || !keys.Any()) return new T[] { };
            return RedisClient.GetAll<T>(keys).Values.ToArray(); ;
        }

        public long IncreaseOne(string key)
        {
            lock (_object)
            {
                return RedisClient.Increment(key, 1);
            }
        }

        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            if (RedisClient != null) RedisClient.Dispose();
        }

    }
}