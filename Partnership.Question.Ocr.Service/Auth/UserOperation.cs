/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       UserOperation
     * 命名空间：       Partnership.Question.Ocr.Service.Auth
     * 创建时间：       5/27/2016 11:41:02 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;
using Partnership.Question.Ocr.Entity.Common;
using Partnership.Question.Ocr.Entity.Entity;
using Partnership.Question.Ocr.Resource.Auth;
using Partnership.Question.Ocr.Service.Encrypter;

namespace Partnership.Question.Ocr.Service.Auth
{
    public class UserOperation
    {
        public RegistResponse RegisterUser(RegistRequest request)
        {
            var user = ConverTotUser(request);
            new RegisterService().Add(user);
            var userToken = ConvertToUserToken(user);
            new UserTokenService().Add(userToken);

            return new RegistResponse {Key = userToken.Key, Token = userToken.Token};
        }

        private static UserToken ConvertToUserToken(User user)
        {
            var userToken = new UserToken();
            var md5Encrypter = new Md5Encrypter();
            userToken.Key =
                md5Encrypter.Get32Md5(string.Format("{0}{1}{2}", user.Phone, md5Encrypter.Get32Md5(user.Password),
                    user.CreateDate));
            userToken.Token = md5Encrypter.Get32Md5(userToken.Key);
            userToken.UserId = user.Id;
            userToken.ClientType = (int) ClinetType.Mobile;
            userToken.CreateDate = DateTime.Now;
            userToken.Expire = -1;
            userToken.IsDeleted = false;
            userToken.Id = Guid.NewGuid().ToString();
            return userToken;
        }
        private static UserToken ConvertToUserToken(string name,string userId, string password, DateTime createDate)
        {
            var userToken = new UserToken();
            var md5Encrypter = new Md5Encrypter();
            userToken.Key =
                md5Encrypter.Get32Md5(string.Format("{0}{1}{2}", name, md5Encrypter.Get32Md5(password),
                    createDate));
            userToken.Token = md5Encrypter.Get32Md5(userToken.Key);
            userToken.UserId = userId;
            userToken.ClientType = (int) ClinetType.Mobile;
            userToken.CreateDate = DateTime.Now;
            userToken.Expire = -1;
            userToken.IsDeleted = false;
            userToken.Id = Guid.NewGuid().ToString();
            return userToken;
        }

        public LoginResponse Login(LoginRequest request, string userId)
        {
            var convertToLoginLog = ConvertToLoginLog(request, userId);
            var loginLog = new LoginLogService().Add(convertToLoginLog);
            var userTokenService = new UserTokenService();
            var userToken = userTokenService.Get(loginLog.Name);
            var convertToUserToken = ConvertToUserToken(request.Name,userId, request.Password, loginLog.CreateDate.Value);
            UserToken token = null;
            if (userToken == null)
            {
                token = userTokenService.Add(convertToUserToken);
            }
            else
            {
                userToken.Key = convertToUserToken.Key;
                userToken.Token = convertToUserToken.Token;
                userToken.UpdateDate = DateTime.Now;
                userToken.Updater = userId;
                token = userTokenService.Update(userToken);
            }

            if (token != null)
            {
                return new LoginResponse()
                {
                    Key = token.Key,
                    Token = token.Token
                };
            }
            return null;
        }

        public ChangePasswordResponse ChangePassword(User user)
        {
            var loginLog = new LoginLogService().Get(user.Id);
            var userTokenService = new UserTokenService();
            var userToken = userTokenService.Get(loginLog.Name);
            var convertToUserToken = ConvertToUserToken(user.Name, user.Id, user.Password, loginLog.CreateDate.Value);
            UserToken token = null;
            if (userToken == null)
            {
                token = userTokenService.Add(convertToUserToken);
            }
            else
            {
                userToken.Key = convertToUserToken.Key;
                userToken.Token = convertToUserToken.Token;
                userToken.UpdateDate = DateTime.Now;
                userToken.Updater = user.Id;
                token = userTokenService.Update(userToken);
            }



            if (token != null)
            {
                return new ChangePasswordResponse()
                {
                    Key = token.Key,
                    Token = token.Token
                };
            }
            return null;
        }

        private LoginLog ConvertToLoginLog(LoginRequest request, string userId)
        {
            var loginLog = new LoginLog();
            loginLog.Id = Guid.NewGuid().ToString();
            loginLog.AppVersion = request.AppVersion;
            loginLog.DeviceId = request.DeviceId;
            loginLog.Ip = request.Ip;
            loginLog.Loaction = request.Loaction;
            loginLog.LocationTime = request.LocationTime;
            loginLog.Name = request.Name;
            loginLog.Password = request.Password;
            loginLog.CreateDate = DateTime.Now;
            loginLog.IsDeleted = false;
            loginLog.Creator = userId;
            loginLog.UserId = userId;

            return loginLog;
        }

        private User ConverTotUser(RegistRequest request)
        {
            var user = new User();
            user.CreateDate = DateTime.Now;
            user.Creator = request.Phone;
            user.Password = new Md5Encrypter().Get32Md5(request.Password);
            user.Address = request.Address;
            user.Birth = request.Birth;
            user.AppVersion = request.AppVersion;
            user.Email = request.Email;
            user.Id = Guid.NewGuid().ToString();
            user.IsDeleted = false;
            user.Name = request.Name;
            user.RegistCourse = request.RegistCourse;
            user.RegistDevice = request.RegistDevice;
            user.RegistDeviceID = request.RegistDeviceID;
            user.RegistLocation = request.RegistLocation;
            user.Sex = request.Sex;
            user.Phone = request.Phone;

            return user;
        }
    }
}