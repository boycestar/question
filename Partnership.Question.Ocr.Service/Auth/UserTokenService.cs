/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       UserTokenService
     * 命名空间：       Partnership.Question.Ocr.Service.Auth
     * 创建时间：       5/27/2016 11:01:16 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using Partnership.Question.Ocr.Entity.Entity;
using Partnership.Question.Ocr.Repository.Auth;

namespace Partnership.Question.Ocr.Service.Auth
{
    public class UserTokenService
    {
        public UserTokenService()
        {
            Repository = new UserTokenRepository();
        }

        public UserToken Add(UserToken enity)
        {
            return Repository.Add(enity);
        }

        public UserToken Update(UserToken enity)
        {
            return Repository.Update(enity);
        }

        public UserToken Get(string name)
        {
            return Repository.Get(name);
        }

        public UserToken Get(string key, string token)
        {
            return Repository.Get(key, token);
        }

        public UserTokenRepository Repository { get; private set; }
    }
}