/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       LoginLogService
     * 命名空间：       Partnership.Question.Ocr.Service.Auth
     * 创建时间：       5/28/2016 11:52:15 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using Partnership.Question.Ocr.Entity.Entity;
using Partnership.Question.Ocr.Repository.Auth;

namespace Partnership.Question.Ocr.Service.Auth
{
    public class LoginLogService
    {
        public LoginLogRespository Repository { get; private set; }

        public LoginLogService()
        {
            Repository = new LoginLogRespository();
        }

        public LoginLog Add(LoginLog enity)
        {
            return Repository.Add(enity);
        }

        public LoginLog Get(string userId)
        {
            return Repository.Get(userId);
        }
    }
}