/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       RegisterService
     * 命名空间：       Partnership.Question.Ocr.Service.Auth
     * 创建时间：       5/27/2016 9:59:38 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using Partnership.Question.Ocr.Entity.Entity;
using Partnership.Question.Ocr.Repository.Auth;

namespace Partnership.Question.Ocr.Service.Auth
{
    public class RegisterService
    {
        public RegisterService()
        {
            Repository = new RegisterRepository();
        }

        public User Add(User enity)
        {
            return Repository.Add(enity);
        }

        public User Get(string name, string password)
        {
            return Repository.Get(name, password);
        }

        public User Get(string name)
        {
            return Repository.Get(name);
        }

        public User Get(string name, string phone, string email)
        {
            return Repository.Get(name, phone, email);
        }

        public User Update(User user)
        {
            return Repository.Update(user);
        }

        public RegisterRepository Repository { get; private set; }
    }
}