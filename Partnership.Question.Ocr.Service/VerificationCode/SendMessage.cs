/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       SendMessage
     * 命名空间：       Partnership.Question.Ocr.Service.VerificationCode
     * 创建时间：       5/25/2016 9:57:37 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;
using System.IO;
using System.Net;
using System.Text;

namespace Partnership.Question.Ocr.Service.VerificationCode
{
    public class SendMessage
    {
        /// <summary>
        /// 发送HTTP请求
        /// </summary>
        /// <param name="phoneNo">发送号码：多个号码用半角逗号隔开</param>
        /// <param name="content">只支持验证码、订单、物流及各种通知类、触发类短信的发送，不支持具有营销性质或者任何非法违规内容；</param>
        /// <returns>请求结果</returns>
        public static string Send(string phoneNo, int content)
        {
            var strValue = "";
            try
            {
                var notice = string.Format("%e3%80%90Mon%e3%80%91%e6%82%a8%e7%9a%84%e7%a1%ae%e8%ae%a4%e7%a0%81%e6%98%af{0}%ef%bc%8c%e6%9c%89%e6%95%88%e6%97%b6%e9%97%b45%e5%88%86%e9%92%9f%ef%bc%8c%e8%af%b7%e4%b8%8d%e8%a6%81%e5%91%8a%e8%af%89%e4%bb%96%e4%ba%ba", content);

                var strUrl =
                        string.Format("http://apis.baidu.com/kingtto_media/106sms/106sms?mobile={0}&content={1}&tag=2", phoneNo,
                            notice);
                HttpWebRequest request;
                request = (HttpWebRequest)WebRequest.Create(strUrl);
                request.Method = "GET";
                // 添加header
                request.Headers.Add("apikey", "38584dc9375eb367a5fabc106329c0b9");
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                Stream s;
                s = response.GetResponseStream();
                var strDate = "";
                var reader = new StreamReader(s, Encoding.UTF8);
                while ((strDate = reader.ReadLine()) != null)
                {
                    strValue += strDate + "\r\n";
                }
            }
            catch (System.Exception ex)
            {
            }
            return strValue;
        }

        /// <summary>
        /// 发送HTTP请求
        /// </summary>
        /// <param name="phoneNo">发送号码：多个号码用半角逗号隔开</param>
        /// <param name="content">只支持验证码、订单、物流及各种通知类、触发类短信的发送，不支持具有营销性质或者任何非法违规内容；</param>
        /// <returns>请求结果</returns>
        public static string Send(string phoneNo, string content)
        {
            var strValue = "";
            try
            {
                var notice = string.Format("%e3%80%90Mon%e3%80%91%e6%82%a8%e7%9a%84%e7%a1%ae%e8%ae%a4%e7%a0%81%e6%98%af{0}%ef%bc%8c%e6%9c%89%e6%95%88%e6%97%b6%e9%97%b45%e5%88%86%e9%92%9f%ef%bc%8c%e8%af%b7%e4%b8%8d%e8%a6%81%e5%91%8a%e8%af%89%e4%bb%96%e4%ba%ba", content);

                var strUrl =
                        string.Format("http://apis.baidu.com/kingtto_media/106sms/106sms?mobile={0}&content={1}&tag=2", phoneNo,
                            notice);
                HttpWebRequest request;
                request = (HttpWebRequest)WebRequest.Create(strUrl);
                request.Method = "GET";
                // 添加header
                request.Headers.Add("apikey", "38584dc9375eb367a5fabc106329c0b9");
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                Stream s;
                s = response.GetResponseStream();
                var strDate = "";
                var reader = new StreamReader(s, Encoding.UTF8);
                while ((strDate = reader.ReadLine()) != null)
                {
                    strValue += strDate + "\r\n";
                }
            }
            catch (System.Exception ex)
            {
            }
            return strValue;
        }

        public static string GenerateCode()
        {
            var random = new Random();
            var next = random.Next(0,9999);
            return string.Format("{0:d4}", next);
        }
    }
}