/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       AesEncrypter
     * 命名空间：       Partnership.Question.Ocr.Service.Encrypter
     * 创建时间：       5/27/2016 11:06:39 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Partnership.Question.Ocr.Interface;

namespace Partnership.Question.Ocr.Service.Encrypter
{
    public class AesEncrypter : IEncrypter
    {
        protected const int BLOCKSIZE = 128;

        public byte[] Encrypt(byte[] buffer, string strKey)
        {
            var aes = new AesCryptoServiceProvider();
            var keyVector = new AesKeyVector(strKey);
            aes.BlockSize = BLOCKSIZE;
            aes.KeySize = keyVector.KeyLength;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.GenerateIV();
            var iv = aes.IV;
            // 入出力用のストリーム
            ICryptoTransform cryptTransform = null;
            var ms = new MemoryStream();
            cryptTransform = aes.CreateEncryptor(keyVector.Key, iv);
            var cs = new CryptoStream(ms, cryptTransform,
                CryptoStreamMode.Write);
            ms.Write(iv, 0, iv.Length);
            // ストリームに暗号化するデータを書き込む
            cs.Write(buffer, 0, buffer.Length);
            cs.Close();
            // 暗号化されたデータを byte 配列で取得
            var bDestination = ms.ToArray();
            ms.Close();
            return bDestination;
        }

        
        public byte[] Encrypt(string strInput, string strKey)
        {
            //暗号化する
            var bSource = Encoding.Unicode.GetBytes(strInput);
            return Encrypt(bSource, strKey);
        }

        
        public string EncryptAndBase64(string strInput, string strKey)
        {
            //暗号化の上、Base64エンコーディングをする
            var bArray = Encrypt(strInput, strKey);
            return Convert.ToBase64String(bArray);
        }

        
        public string Decrypt(byte[] bInput, string strKey)
        {
            //復号する
            var bDecrypted = BinaryDecrypt(bInput, strKey);
            return Encoding.Unicode.GetString(bDecrypted);
        }

        
        public byte[] BinaryDecrypt(byte[] bInput, string strKey)
        {
            var aes = new AesCryptoServiceProvider();
            var keyVector = new AesKeyVector(strKey);
            aes.BlockSize = BLOCKSIZE;
            aes.KeySize = keyVector.KeyLength;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            var ms = new MemoryStream();
            ICryptoTransform cryptTransform = null;
            var iv = new byte[BLOCKSIZE/8];
            for (var i = 0; i < iv.Length; i++)
            {
                iv[i] = bInput[i];
            }
            cryptTransform = aes.CreateDecryptor(keyVector.Key, iv);
            var cs = new CryptoStream(ms, cryptTransform
                , CryptoStreamMode.Write);
            cs.Write(bInput, (BLOCKSIZE/8), bInput.Length - (BLOCKSIZE/8));
            cs.Close();
            var bDecrypted = ms.ToArray();
            ms.Close();
            return bDecrypted;
        }

        
        public string DecryptFromBase64(string strInput, string strKey)
        {
            //Base64からデコードの上、復号する
            var bs = Convert.FromBase64String(strInput);
            return Decrypt(bs, strKey);
        }
    }

    internal class AesKeyVector
    {
        private static readonly int[] ALLOWEDKEYSIZE = {128, 192, 256};
        private int m_iKeyLength = 256; 
        
        public AesKeyVector()
        {
            //ディフォルトコンストラクタ
        }

        
        public AesKeyVector(string strKey)
        {
            //コンストラクタ
            KeyString = strKey;
        }

        
        public AesKeyVector(string strKey, int iLength)
        {
            ////コンストラクタ
            KeyString = strKey;
            KeyLength = iLength;
        }

        
        public string KeyString { get; set; }
        
        public int KeyLength
        {
            get { return m_iKeyLength; }
            set
            {
                var bValidValue = false;
                for (var i = 0; i < ALLOWEDKEYSIZE.Length; i++)
                {
                    if (ALLOWEDKEYSIZE[i] == value)
                    {
                        m_iKeyLength = value;
                        bValidValue = true;
                        break;
                    }
                }
                if (!bValidValue)
                {
                    var strMessage = "キー長が不正です。";
                    strMessage += value.ToString();
                    throw new Exception("长度不正确");
                }
            }
        }

        
        public byte[] Key
        {
            //暗号化キー
            get
            {
                var iByteLength = m_iKeyLength/8;
                var key = new byte[iByteLength];
                var bPassword = Encoding.UTF8.GetBytes(KeyString);
                SHA1 shaM = new SHA1Managed();
                var bStream = shaM.ComputeHash(bPassword);
                for (var i = 0; i < iByteLength; i++)
                {
                    key[i] = 0;
                }
                var iLimit = (bStream.Length <= iByteLength)
                    ? bStream.Length
                    : iByteLength;
                for (var i = 0; i < iLimit; i++)
                {
                    key[i] = bStream[i];
                }
                return key;
            }
        }
    }
}