/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       Md5Encrypter
     * 命名空间：       Partnership.Question.Ocr.Service.Encrypter
     * 创建时间：       5/27/2016 11:28:05 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System.Security.Cryptography;
using System.Text;

namespace Partnership.Question.Ocr.Service.Encrypter
{
    public class Md5Encrypter
    {
        /// <summary>
        ///     MD5获取32位加密算法
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string Get32Md5(string source)
        {
            var txtEncrypt = new StringBuilder();
            var md5 = MD5.Create(); //实例化一个md5对像
            // 加密后是一个字节类型的数组，这里要注意编码UTF8/Unicode等的选择　
            var s = md5.ComputeHash(Encoding.UTF8.GetBytes(source));
            // 通过使用循环，将字节类型的数组转换为字符串，此字符串是常规字符格式化所得
            for (var i = 0; i < s.Length; i++)
            {
                // 将得到的字符串使用十六进制类型格式。格式后的字符是小写的字母，如果使用大写（X）则格式后的字符是大写字符 

                txtEncrypt.Append(s[i].ToString("X"));
            }
            return txtEncrypt.ToString();
        }
    }
}