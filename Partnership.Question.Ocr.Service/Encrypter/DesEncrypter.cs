/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       DesEncrypter
     * 命名空间：       Partnership.Question.Ocr.Service.Encrypter
     * 创建时间：       5/27/2016 11:09:13 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Partnership.Question.Ocr.Interface;

namespace Partnership.Question.Ocr.Service.Encrypter
{
    public class DesEncrypter : IEncrypter
    {
        public byte[] Encrypt(byte[] buffer, string strKey)
        {
            var des = new TripleDESCryptoServiceProvider();
            // 入出力用のストリームを生成します
            des.GenerateIV();
            var iv = des.IV;
            ICryptoTransform cryptTransform = null;
            var ms = new MemoryStream();
            var keyVector = new KeyVector(strKey);
            cryptTransform = des.CreateEncryptor(keyVector.Key, iv);
            var cs = new CryptoStream(ms, cryptTransform,
                CryptoStreamMode.Write);
            ms.Write(iv, 0, iv.Length);
            // ストリームに暗号化するデータを書き込む
            cs.Write(buffer, 0, buffer.Length);
            cs.Close();
            // 暗号化されたデータを byte 配列で取得
            var bDestination = ms.ToArray();
            ms.Close();
            return bDestination;
        }

        
        public byte[] Encrypt(string strInput, string strKey)
        {
            //暗号化する
            var bSource = Encoding.Unicode.GetBytes(strInput);
            return Encrypt(bSource, strKey);
        }

        
        public string EncryptAndBase64(string strInput, string strKey)
        {
            //暗号化の上、Base64エンコーディングをする
            var bArray = Encrypt(strInput, strKey);
            return Convert.ToBase64String(bArray);
        }

        
        public string Decrypt(byte[] bInput, string strKey)
        {
            //復号する
            var bDecrypted = BinaryDecrypt(bInput, strKey);
            return Encoding.Unicode.GetString(bDecrypted);
        }

        
        public byte[] BinaryDecrypt(byte[] bInput, string strKey)
        {
            var des = new TripleDESCryptoServiceProvider();
            des.GenerateIV();
            var iv = des.IV;
            var keyVector = new KeyVector(strKey);
            var ms = new MemoryStream();
            ICryptoTransform cryptTransform = null;
            for (var i = 0; i < iv.Length; i++)
            {
                iv[i] = bInput[i];
            }
            cryptTransform = des.CreateDecryptor(keyVector.Key, iv);
            var cs = new CryptoStream(ms, cryptTransform
                , CryptoStreamMode.Write);
            cs.Write(bInput, iv.Length, (bInput.Length - iv.Length));
            cs.Close();
            var bDecrypted = ms.ToArray();
            ms.Close();
            return bDecrypted;
        }

        
        public string DecryptFromBase64(string strInput, string strKey)
        {
            //Base64からデコードの上、復号する
            var bs = Convert.FromBase64String(strInput);
            return Decrypt(bs, strKey);
        }
    }

    internal class KeyVector
    {
        private static readonly int[] ALLOWEDKEYSIZE = {128, 192};
        private int m_iKeyLength = 192; //only 128, 192//鍵長
        
        public KeyVector()
        {
            //ディフォルトコンストラクタ
        }

        
        public KeyVector(string strKey)
        {
            //コンストラクタ
            KeyString = strKey;
        }

        
        public KeyVector(string strKey, int iLength)
        {
            ////コンストラクタ
            KeyString = strKey;
            KeyLength = iLength;
        }

        
        public string KeyString { get; set; }
        
        public int KeyLength
        {
            get { return m_iKeyLength; }
            set
            {
                var bValidValue = false;
                for (var i = 0; i < ALLOWEDKEYSIZE.Length; i++)
                {
                    if (ALLOWEDKEYSIZE[i] == value)
                    {
                        m_iKeyLength = value;
                        bValidValue = true;
                        break;
                    }
                }
                if (!bValidValue)
                {
                    var strMessage = "长度不正确";
                    strMessage += value.ToString();
                    throw new Exception("长度不正确");
                }
            }
        }

        
        public byte[] Key
        {
            //暗号化キー
            get
            {
                var iByteLength = m_iKeyLength/8;
                var key = new byte[iByteLength];
                for (var i = 0; i < iByteLength; i++)
                {
                    key[i] = (byte) (KeyString[i%KeyString.Length] + i);
                }
                return key;
            }
        }

        
        public byte[] IV
        {
            //IV
            get
            {
                var iByteLength = m_iKeyLength/8;
                var iv = new byte[iByteLength];
                for (var i = 0; i < iByteLength; i++)
                {
                    iv[i] = (byte) (KeyString[i%KeyString.Length] + (-1)*i);
                }
                return iv;
            }
        }
    }
}