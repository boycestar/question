/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       RC4Encrypter
     * 命名空间：       Partnership.Question.Ocr.Service.Encrypter
     * 创建时间：       5/27/2016 11:10:40 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using Partnership.Question.Ocr.Interface;
using System;
using System.Text;

namespace Partnership.Question.Ocr.Service.Encrypter
{
    public class RC4Encrypter : IEncrypter
    {
        public byte[] Encrypt(byte[] buffer, string strKey)
        {
            var bKey = Encoding.UTF8.GetBytes(strKey);
            return EncryptOutput(bKey, buffer);
        }

        
        public byte[] Encrypt(string strInput, string strKey)
        {
            var bSource = Encoding.Unicode.GetBytes(strInput);
            return Encrypt(bSource, strKey);
        }

        
        public string EncryptAndBase64(string strInput, string strKey)
        {
            var bArray = Encrypt(strInput, strKey);
            return Convert.ToBase64String(bArray);
        }

        
        public string Decrypt(byte[] bInput, string strKey)
        {
            var bDecrypted = BinaryDecrypt(bInput, strKey);
            return Encoding.Unicode.GetString(bDecrypted);
        }

        
        public byte[] BinaryDecrypt(byte[] bInput, string strKey)
        {
            return Encrypt(bInput, strKey);
        }

        
        public string DecryptFromBase64(string strInput, string strKey)
        {
            var bs = Convert.FromBase64String(strInput);
            return Decrypt(bs, strKey);
        }

        
        private static byte[] EncryptOutput(byte[] key, byte[] data)
        {
            var s = EncryptInitalize(key);
            var i = 0;
            var j = 0;
            for (var idx = 0; idx < data.Length; idx++)
            {
                i = (i + 1) & 255;
                j = (j + s[i]) & 255;
                Swap(s, i, j);
                var iTempIndex = (s[i] + s[j]) & 255;
                data[idx] = (byte) (data[idx] ^ s[iTempIndex]);
            }
            return data;
        }

        
        private static byte[] EncryptInitalize(byte[] key)
        {
            var s = new byte[256];
            for (var i = 0; i < 256; i++)
            {
                s[i] = (byte) i;
            }
            for (int i = 0, j = 0; i < 256; i++)
            {
                j = (j + key[i%key.Length] + s[i]) & 255;
                Swap(s, i, j);
            }
            return s;
        }

        
        private static void Swap(byte[] s, int i, int j)
        {
            var c = s[i];
            s[i] = s[j];
            s[j] = c;
        }
    }
}