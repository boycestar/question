/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       LuceneSearch
     * 命名空间：       Partnership.Question.Ocr.Service.Search
     * 创建时间：       5/22/2016 9:03:06 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System.Collections.Generic;
using Lucene.Net.Analysis.PanGu;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Lucene.Net.Util;
using Partnership.Question.Ocr.Entity.Entity;
using ServiceStack.Configuration;

namespace Partnership.Question.Ocr.Service.Search
{
    public class LuceneSearch
    {
        public IList<TExamSubjects> SearchExamSubjects(string words, int pageIndex, int pageSize, out int total)
        {
            total = 0;
            var examSubjectses = new List<TExamSubjects>();
            var indexStorePath = ConfigUtils.GetAppSetting("INDEX_STORE_PATH");
            if (string.IsNullOrWhiteSpace(indexStorePath))
                return examSubjectses;
            var search = new IndexSearcher(FSDirectory.Open(indexStorePath), true);


            var segmentWords = LuceneUtil.GetSegmentWords(words, new PanGuTokenizer());
            var bq = new BooleanQuery();
            if (!string.IsNullOrEmpty(segmentWords))
            {
                // 关键字搜索 SbjContent
                QueryParser queryParser = new MultiFieldQueryParser(Version.LUCENE_30, new[] {"SbjContent"},
                    new PanGuAnalyzer());
                var query = queryParser.Parse(segmentWords);
                bq.Add(query, Occur.MUST);
            }

            // 按 SbjId 字段来排序
            var sorts = new List<SortField>();
            var sf = new SortField("SbjId", SortField.DOUBLE, true);
            sorts.Add(sf);
            var sort = new Sort(sorts.ToArray());
            var docs = search.Search(bq, null, search.MaxDoc, sort);


            var subjectses = new List<TExamSubjects>();

            // 按页匹配数据
            var scoreDocs = LuceneUtil.TakeScoreDocs(pageIndex, pageSize, out total, docs);

            //遍历搜索到的结果
            foreach (var scoreDoc in scoreDocs)
            {
                try
                {
                    var doc = search.Doc(scoreDoc.Doc);
                    var model = new TExamSubjects();
                    model.SbjId = int.Parse(doc.Get("SbjId"));
                    model.SbjContent = doc.Get("SbjContent");
                    model.RightAnswer = doc.Get("RightAnswer");
                    model.Explain = doc.Get("Explain");
                    subjectses.Add(model);
                }
                catch
                {
                }
            }
            search.Close();
            search.Dispose();

            return subjectses;
        }
    }
}