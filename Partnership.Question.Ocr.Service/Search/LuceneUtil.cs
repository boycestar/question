/*************************************************************************************
     * CLR 版本：       4.0.30319.42000
     * 类 名 称：       LuceneUtil
     * 命名空间：       Partnership.Question.Ocr.Service.Search
     * 创建时间：       5/22/2016 9:26:40 PM
     * 作    者：       boyce
     * 邮    箱:        boycestar@hotmail.com
     * 说    明：       
     * 修改时间：
     * 修 改 人：
*************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Analysis.PanGu;
using Lucene.Net.Search;

namespace Partnership.Question.Ocr.Service.Search
{
    public class LuceneUtil
    {
        public static string GetSegmentWords(string keywords, PanGuTokenizer ktTokenizer)
        {
            var result = new StringBuilder();

            // 执行分词操作 一个关键字可以拆分为多个次和单个字
            var words = ktTokenizer.SegmentToWordInfos(keywords);

            foreach (var word in words)
            {
                if (word == null)
                {
                    continue;
                }

                result.AppendFormat("{0} ", word.Word);
            }

            return result.ToString().Trim();
        }

        public static IList<ScoreDoc> TakeScoreDocs(int pageIndex , int pageSize, out int total, TopFieldDocs docs)
        {
            total = docs.TotalHits;
            return docs.ScoreDocs.Skip((pageIndex-1)*pageSize).Take(pageSize).ToList();
        }
    }
}